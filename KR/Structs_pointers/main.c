#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
    char name[80];
    int year;
    struct car
    {
        char model[80];
        int year;
    } car;
};
void displayPersonInfo(struct person * s);
void displayAllPeople(struct person people [], int size);
struct people * defaultArray(struct person people [], int size);
struct people * fillInfo(struct person *p, char* name, int year, char * model, int yearOfprod);
struct people ** findPeople(struct person people [], int size);
int main()
{
    int n = 0;
    printf("Please, enter integer n:\n");
    scanf("%i", &n);
    struct person* array = (struct person*)malloc(n*sizeof(struct person));
    defaultArray(array,n);
    printf("\nDefault array:\n\n");
    displayAllPeople(array, n);
    printf("Tests:\n\n");
    fillInfo(&array[0], "Taras", 18, "BMW", 2000);
    fillInfo(&array[1], "Masha", 25, "Ford", 2005);
    fillInfo(&array[2], "Oleg", 23, "Citroen", 2010);
    displayAllPeople(array, n);
    printf("Cars older than 5 years:\n\n");
    findPeople(array, n);
    free(array);
    return 0;
}

void displayPersonInfo(struct person * p)
{
    printf("Name: %s\n", p->name);
    printf("%i years old\n", p->year);
    printf("Car: %s\n", p->car.model);
    printf("Year of production: %i\n", p->car.year);
}
void displayAllPeople(struct person people [], int size)
{
    int i = 0;
    for (i = 0; i< size; i++)
    {
        displayPersonInfo(&people[i]);
        printf("------\n");
    }
}
struct people * defaultArray(struct person people [], int size)
{
    int i = 0;
    for (i = 0; i< size; i++)
    {
        people[i].name[0] = '\0';
        people[i].year = 0;
        people[i].car.model[0] = '\0';
        people[i].car.year = 0;
    }
    return people;
}
struct people * fillInfo(struct person *p, char* name, int year, char * model, int yearOfprod)
{
    strcpy(p->name, name);
    p->year = year;
    strcpy(p->car.model, model);
    p->car.year = yearOfprod;
    return p;
}

struct people ** findPeople(struct person people [], int size)
{
    int i = 0;
    for(i = 0; i< size; i++)
    {
        if (people[i].year < 20 && (2016-people[i].car.year) > 5)
        {
            displayPersonInfo(&people[i]);
        }
    }
    return people;
}
