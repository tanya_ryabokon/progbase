#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    size = 200
};

int readFile(const char * readFileName, const char * writeFileName);
int wordLen (char * str, int len);

int main()
{
    const char * readfilename = "in.txt";
    const char * writefilename = "out.txt";
    readFile(readfilename, writefilename);

    return EXIT_SUCCESS;
}

int readFile(const char * readfile, const char * writefile)
{
    char buf[size];
    int div = -1;
    FILE * fin = fopen(readfile, "r");
    FILE * fout = fopen(writefile, "w");
    int i;
    if (fin == NULL)
    {
        printf("File can't be open %s\n", readfile);
        return 1;
    }
    if (fout == NULL)
    {
        printf("File can't be open %s\n", writefile);
        return 1;
    }
    fgets(buf, size, fin);
    while (!feof(fin))
    {
        if (buf[0] !='\n')
        {
            div = wordLen(buf, div);
            fprintf(fout, "%d\n", div);
        }
        fgets(buf, size, fin);
    }
    fclose(fin);
    fclose(fout);
    return 0;
}

int wordLen (char * str, int div)
{
    int min = strlen(str);
    int max = 0;
    char *p = NULL;
    for(p = strtok(str, " "); p; p = strtok(NULL, " "))
    {
        if(strlen(p) < min)
            min = strlen(p);
         if(strlen(p) > max)
            max = strlen(p);
    }
    return max - min;
}
