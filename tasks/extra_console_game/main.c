#include <stdio.h>
#include <time.h>
#include <math.h>
#define ARRAY_SIZE 310
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

#define UP (char)'A'
#define LEFT (char)'D'
#define RIGHT (char)'C'
#define DOWN (char)'B'
#define ENTER 10
#define EXIT 27
#define PAUSE 112

const char HEAD = '*';
const char BODY = 'o';
const char WALL = '#';
const char FOOD = '@';
const char BLANK = ' ';

void gotoxy(int x,int y)
{
	printf("%c[%d;%df",0x1B,y,x);
}

int kbhit(void)
{
 struct termios oldt, newt;
	  int ch;
	  int oldf;

	  tcgetattr(STDIN_FILENO, &oldt);
	  newt = oldt;
	  newt.c_lflag &= ~(ICANON | ECHO);
	  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

	  ch = getchar();

	  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	  fcntl(STDIN_FILENO, F_SETFL, oldf);

	  if(ch != EOF)
	  {
		ungetc(ch, stdin);
		return 1;
	  }

	  return 0;
	}

	char getch()
	{
		char c;
		system("stty raw");
		c= getchar();
		system("stty sane");
		return(c);
	}

	void clrscr()
	{
		system("clear");
		return;
	}
void enviroment(int consoleW, int consoleH)
{
	int i;
	int x = 1, y = 1;
	int rectangleHeight = consoleH - 4;
	clrscr();

	gotoxy(x,y);

	for (; y < rectangleHeight; y++)
	{
		gotoxy(x, y);
		printf("%c",WALL);

		gotoxy(consoleW, y);
		printf("%c",WALL);
	}

	y = 1;
	for (; x < consoleW+1; x++)
	{
		gotoxy(x, y);
		printf("%c",WALL);

		gotoxy(x, rectangleHeight);
		printf("%c",WALL);
	}
	return;
}
char waitForKey(void)
{
	int pressed;

	while(!kbhit());

	pressed = getch();
	return((char)pressed);
}

int getSpeed(void)
{
	int speed;
	clrscr();

	do
	{
		gotoxy(10,5);
		printf("Select level from 1 to 9: ");
		speed = waitForKey()-48;
	} while(speed < 1 || speed > 9);
	return(speed);
}

void pauseDis(void)
{
	int i;

	gotoxy(28,23);
	printf("Paused");
	waitForKey();
	gotoxy(28,23);
	printf("            ");


	return;
}

int isPressed(int dir)
{
	int pressed;

	if(kbhit())
	{
		pressed=getch();
		if (dir != pressed)
		{
			if(pressed == DOWN && dir != UP)
				dir = pressed;
			else if (pressed == UP && dir != DOWN)
				dir = pressed;
			else if (pressed == LEFT && dir != RIGHT)
				dir = pressed;
			else if (pressed == RIGHT && dir != LEFT)
				dir = pressed;
			else if (pressed == EXIT || pressed == PAUSE)
				pauseDis();
		}
	}
	return(dir);
}

int collisionSnake (int x, int y, int snakeCoords[][ARRAY_SIZE], int sLength, int det)
{
	int i;
	for (i = det; i < sLength; i++)
	{
		if ( x == snakeCoords[0][i] && y == snakeCoords[1][i])
			return(1);
	}
	return(0);
}

int Food(int foodCoords[], int width, int height, int snakeCoords[][ARRAY_SIZE], int sLength)
{
	int i;

	do
	{
		srand ( time(NULL) );
		foodCoords[0] = rand() % (width-2) + 2;
		srand ( time(NULL) );
		foodCoords[1] = rand() % (height-6) + 2;
	} while (collisionSnake(foodCoords[0], foodCoords[1], snakeCoords, sLength, 0));

	gotoxy(foodCoords[0] ,foodCoords[1]);
	printf("%c", FOOD);

	return(0);
}


void moveSnake(int snakeCoords[][ARRAY_SIZE], int sLength, int dir)
{
	int i;
	for( i = sLength-1; i >= 1; i-- )
	{
		snakeCoords[0][i] = snakeCoords[0][i-1];
		snakeCoords[1][i] = snakeCoords[1][i-1];
	}

	switch(dir)
	{
		case DOWN:
			snakeCoords[1][0]++;
			break;
		case RIGHT:
			snakeCoords[0][0]++;
			break;
		case UP:
			snakeCoords[1][0]--;
			break;
		case LEFT:
			snakeCoords[0][0]--;
			break;
	}

	return;
}

void move(int snakeCoords[][ARRAY_SIZE], int sLength, int dir)
{
	int x;
	int y;

	x = snakeCoords[0][sLength-1];
	y = snakeCoords[1][sLength-1];

	gotoxy(x,y);
	printf("%c",BLANK);
	gotoxy(snakeCoords[0][0],snakeCoords[1][0]);
	printf("%c", BODY);

	moveSnake(snakeCoords, sLength, dir);

	gotoxy(snakeCoords[0][0],snakeCoords[1][0]);
	printf("%c",HEAD);

	gotoxy(1,1);

	return;
}

int eat(int snakeCoords[][ARRAY_SIZE], int foodCoords[])
{
	if (snakeCoords[0][0] == foodCoords[0] && snakeCoords[1][0] == foodCoords[1])
	{
		foodCoords[0] = 0;
		foodCoords[1] = 0;
		return(1);
	}

	return(0);
}

int collisionDetion(int snakeCoords[][ARRAY_SIZE], int consoleW, int consoleH, int sLength )
{
	int colision = 0;
	if ((snakeCoords[0][0] == 1) || (snakeCoords[1][0] == 1) || (snakeCoords[0][0] == consoleW) || (snakeCoords[1][0] == consoleH - 4)) //Checks if the snake collided wit the wall or it's self
		colision = 1;
	else
		if (collisionSnake(snakeCoords[0][0], snakeCoords[1][0], snakeCoords, sLength, 1))
			colision = 1;

	return(colision);
}

void refreshInfo(int score, int speed)
{
    enviroment(80, 25);
	gotoxy(5,23);
	printf("Score: %i", score);

	gotoxy(5,24);
	printf("Speed: %i\n", speed);
    printf("                ");
	return;
}


void displayScores(int score)
{
	FILE *fp;
	char str[128];
	char name[128];
	int y = 5;
	clrscr();
	if (score != -1){
	fp = fopen("highscores.txt", "a");
    printf("Please enter your name: ");
    scanf("%[^\n]", name);
	fprintf(fp, "%s\t %i\n", name, score);
    fclose(fp);
    }
    fp = fopen("highscores.txt", "r");
	gotoxy(10,y++);
	printf("Scores");
	gotoxy(10,y++);
	printf("Name\t Score");
	while(!feof(fp)) {
		gotoxy(10,y++);
		if(fgets(str, 126, fp))
			printf("%s", str);
	}

	fclose(fp);
	gotoxy(10,y++);

	printf("Press any key to continue...");
	waitForKey();
	return;
}

void gameOverScreen(void)
{
	int x = 17, y = 3;

	gotoxy(30,10);
	printf(".::GAME OVER::.");


	waitForKey();
	clrscr();
	return;
}

void startGame( int snakeCoords[][ARRAY_SIZE], int foodCoords[], int consoleW, int consoleH, int sLength, int dir, int score, int speed)
{
	int gameOver = 0;
	clock_t endWait;

	int waitMili = CLOCKS_PER_SEC-(speed)*(CLOCKS_PER_SEC/10)+10000;
	int oldDir;
	int canChangeDir = 1;
	int tempScore = 10*speed;

	endWait = clock() + waitMili;

	do
	{
		if(canChangeDir)
		{
			oldDir = dir;
			dir = isPressed(dir);
		}

		if(oldDir != dir)
			canChangeDir = 0;

		if(clock() >= endWait)
		{
			move(snakeCoords, sLength, dir);
			canChangeDir = 1;


			if(eat(snakeCoords, foodCoords))
			{
				Food( foodCoords, consoleW, consoleH, snakeCoords, sLength);
				sLength++;
				score+=1;
				if( score >= 10*speed+tempScore)
				{
					speed++;
					tempScore = score;

					if(speed <= 9)
						waitMili = waitMili - (CLOCKS_PER_SEC/10);
					else
					{
						if(waitMili >= 40)
							waitMili = waitMili - (CLOCKS_PER_SEC/200);

					}

				}

				refreshInfo(score, speed);
			}

			endWait = clock() + waitMili;
		}

		gameOver = collisionDetion(snakeCoords, consoleW, consoleH, sLength);

		if(sLength >= ARRAY_SIZE-5)
		{
			gameOver = 2;
		}

	} while (!gameOver);

	switch(gameOver)
	{
		case 1:
			gameOverScreen();
			break;
	}

	if( score != 0)
	{
		displayScores(score);
	}

	return;
}

void loadSnake(int snakeCoords[][ARRAY_SIZE], int sLength)
{
	int i;
	for (i = 0; i < sLength; i++)
	{
		gotoxy(snakeCoords[0][i], snakeCoords[1][i]);
		printf("%c", BODY);
	}

	return;
}

void prepairSnakeArr(int snakeCoords[][ARRAY_SIZE], int sLength)
{
	int i, x;
	int snakeX = snakeCoords[0][0];
	int snakeY = snakeCoords[1][0];

	for(i = 1; i <= sLength; i++)
	{
		snakeCoords[0][i] = snakeX + i;
		snakeCoords[1][i] = snakeY;
	}

	return;
}

void loadGame(void)
{
	int snakeCoords[2][ARRAY_SIZE];
	int sLength = 4;
	int dir = LEFT;
	int foodCoords[] = {5,5};
	int score = 0;
	int consoleW = 80;
	int consoleH = 25;

	int speed = getSpeed();

	snakeCoords[0][0] = 40;
	snakeCoords[1][0] = 10;

	enviroment(consoleW, consoleH);
	prepairSnakeArr(snakeCoords, sLength);
	loadSnake(snakeCoords, sLength);
	Food( foodCoords, consoleW, consoleH, snakeCoords, sLength);
	refreshInfo(score, speed);
	startGame(snakeCoords, foodCoords, consoleW, consoleH, sLength, dir, score, speed);

	return;
}

int Selector(int x, int y, int yStart)
{
	char key;
	int i = 0;
	x = x - 2;
	gotoxy(x,yStart);

	printf(">");

	gotoxy(1,1);


	do
	{
		key = waitForKey();
		if ( key == (char)UP )
		{
			gotoxy(x,yStart+i);
			printf(" ");

			if (yStart >= yStart+i )
				i = y - yStart - 2;
			else
				i--;
			gotoxy(x,yStart+i);
			printf(">");
		}
		else
			if ( key == (char)DOWN )
			{
				gotoxy(x,yStart+i);
				printf(" ");

				if (i+2 >= y - yStart )
					i = 0;
				else
					i++;
				gotoxy(x,yStart+i);
				printf(">");
			}
	} while(key != (char)ENTER);
	return(i);
}

void welcome(void)
{
	clrscr();
	gotoxy(10, 10);
	printf("\t		.::Snake::.		\n");
	printf("\t                  To start press any key \n");
	printf("\n");

	waitForKey();
	return;
}


void exitGame(void)
{
		clrscr();
		exit(1);
	return;
}

int mainMenu(void)
{
    clrscr();
	int x = 10, y = 5;
	int yStart = y;

	int selected;

	clrscr();
	gotoxy(x,y++);
	printf("New Game\n");
	gotoxy(x,y++);
	printf("Scores\n");
	gotoxy(x,y++);
	printf("Exit\n");
	gotoxy(x,y++);

	selected = Selector(x, y, yStart);

	return(selected);
}

int main()
{

	welcome();

	do
	{
        clrscr();
		switch(mainMenu())
		{
			case 0:
				loadGame();
				break;
			case 1:
				displayScores(-1);
				break;
			case 2:
				exitGame();
				break;
		}
	} while(1);

	return(0);
}
