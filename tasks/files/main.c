#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

enum {
    BUFFER_SIZE = 100
};

int file_process(const char * readFileName, const char * writeFileName);

int main()
{
    const char * readfilename = "fin.txt";
    const char * writefilename = "fout.txt";
    file_process(readfilename, writefilename);
    return EXIT_SUCCESS;
}

int file_process(const char * readFileName, const char * writeFileName)
{
    char buffer[BUFFER_SIZE];
    int i;
    FILE * fin = fopen(readFileName, "r");
    FILE * fout = fopen(writeFileName, "w");
    if (fin == NULL)
    {
        printf("Error opening file %s\n", readFileName);
        return 1;
    }
    if (fout == NULL)
    {
        printf("Error opening file %s\n", writeFileName);
        return 1;
    }
    fprintf(fout, "Print in a row all the characters of the beginning of all non-empty strings in the text.\n\n");
    fgets(buffer, BUFFER_SIZE, fin);
    while (!feof(fin))
    {
        if (buffer[0] !='\n')
             fputc(buffer[0], fout);
        fgets(buffer, BUFFER_SIZE, fin);

    }
    fclose(fin);
    fclose(fout);
    return 0;
}
