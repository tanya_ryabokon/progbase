#include <stdio.h>

#include <progbase.h>
#include <pbconsole.h>

int main(void) 
{
	 
    int i = 0; 
    int x =0;
    int y = 0;
    int len = 0;
    int shiftx = 2;
	int shifty = 2;
    len = 6;
    conReset();
	conClear();
	
	conSetAttr(BG_WHITE);
	 for (y = 2; y < 18; y++)
    {
   	 for (x = 2; x < 30; x++)
   	 {
   		 conMove(y, x);
   		 putchar(' ');
   	 }
    }

	
	/* Green part  */
	shiftx = 7;
	shifty = 4;
	conSetAttr(BG_GREEN);

	for (y =1; y<=4; y++)
	{
		for (i = 0; i < len; i++)
		{
			conMove(shiftx+i, shifty);
			putchar(' ');
		}
		shiftx--;
		shifty += 1;
		len +=2;
	}

	shiftx++;
	for (i = 0; i < len-2; i++)
		{
			conMove(shiftx+i, shifty);
			putchar(' ');
		}
			
	len = 13;
	shiftx = 5;
	shifty = 9;

	for (y =0; y<=3; y++)
	{
	if (y==2) len++;
		for (i = 0; i < len-y-1; i++)
		{
			conMove(shiftx+i, shifty);
			putchar(' ');
			
		}
		shiftx += 1;
		shifty += 1;
	}

	shiftx = 11;
	shifty = 13;
	len=6;

	for (i = 0; i < len; i++)
		{
			conMove(shiftx+i, shifty);
			putchar(' ');
			
		}
	shiftx += 1;
	shifty += 1; 


	len = 4;
	for (y =1; y<=4; y++)
	{
		for (i = 0; i <= len-y; i++)
		{
			conMove(shiftx+i, shifty);
			putchar(' ');
		}
		shifty++;
	}
	/* Blue part */
	conSetAttr(BG_BLUE);
	shiftx = 8;
	shifty = 14;


	for (y = shiftx; y < shiftx+4; y++)
    {
   	 for (i = shifty; i < shifty+4; i++)
   	 {
   		 conMove(y, i);
   		 putchar(' ');
   	 }
    }
		
	shiftx = 9;
	shifty = 13;

	for (y = shiftx; y < shiftx+2; y++)
    {
   	 for (i = shifty; i < shifty+6; i++)
   	 {
   		 conMove(y, i);
   		 putchar(' ');
   	 }
    }

    /* Red part*/
    conSetAttr(BG_RED);
    shiftx = 8;
	shifty = 13;
	conMove(shiftx, shifty);
     putchar(' ');
     shiftx = 7;
	shifty = 12;

	for (i = 0; i < 8; i++)
   	 {
   		 conMove(shiftx, shifty+i);
   		 putchar(' ');
   	 }
   	 shiftx --;
   	 shifty--;
   	 len = 16;
     for (y =1; y<=4; y++)
	{
		for (i = 0; i < len; i++)
		{
			conMove(shiftx, shifty+i);
			putchar(' ');
		}
		shiftx--;
		shifty --;
		if (len<17)
		len ++;
	}
	shifty +=2;

	for (i = 0; i < 15; i++)
   	 {
   		 conMove(shiftx, shifty+i);
   		 putchar(' ');
   	 }
   	 
   	 /*Yellow part */
   	 
   	 conSetAttr(BG_YELLOW);
   	 shiftx = 17;
	shifty = 13;
	len = 8;

	for (y =1; y<=6; y++)
	{
		for (i = 0; i < len+1; i++)
		{
			conMove(shiftx, shifty+i);
			putchar(' ');
		}
		shiftx--;
		shifty ++;
		if (y<3)
		len ++;
	}
	shifty--;
	for (y=0; y<2; y++)
	{
   	 for (i = 0; i < len+1; i++)
		{
			conMove(shiftx, shifty+i);
			putchar(' ');
		}
		shiftx-=3;
	}
	shiftx =7;
	shifty =20;
	for (i = 0; i < 9; i++)
		{
			conMove(shiftx, shifty+i);
			putchar(' ');
		}
		
	shiftx =9;
	shifty =19;
	for (i = 0; i < 10; i++)
		{
			conMove(shiftx, shifty+i);
			putchar(' ');
			conMove(shiftx+1, shifty+i);
			putchar(' ');
		}
		shiftx =6;
	shifty =27;
	conMove(shiftx, shifty);
			putchar(' ');
	conReset(); 
	conMove(18, 1);
	return 0;
}
