#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void){
    /* pointers declaration */
    char * inputStr     = NULL;

    /* memory allocation */
    inputStr = (char *) malloc(50 * sizeof(char));           /* alloc memory for input string */
    if (NULL == inputStr){
        printf("Alloc error");
        return EXIT_FAILURE;
    }

    /* assignments */
    printf("Find and display the character following the last character '@'\n");
    printf("Please, input string: ");
    scanf("%[^\n\r]", inputStr);
    printf("The entered string: %s\n", inputStr);

    /* algorithm */
    char * chr = NULL;
    chr = strrchr(inputStr, '@');  
    if ((chr - inputStr + 1) != strlen(inputStr) && chr != NULL)
    {
        *chr++;
        /* print result */
    	printf("Found character: %c\n", *chr);
    }
    else
    {
        chr = (char *) malloc(1 * sizeof(char));
        *chr = '0';
        /* print result */
   		 printf("Found character: %c\n", *chr);
   		 free(chr);
    }

    /* memory deallocation */
    free(inputStr);          /* for malloc */

    return EXIT_SUCCESS;
}
