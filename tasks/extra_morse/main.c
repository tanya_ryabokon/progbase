#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define __LEN(X) sizeof(X)/sizeof(X[0])
#define __BOOLSTR(X) ((X) ? "TRUE" : "FALSE")

struct TestCase {
    const char * input;
    int unit_len;
    int pad_len;
    char * output;
};

 struct Dict {
    char l[1];
    char code[10];
};
const struct Dict dict[40] = {
    {"A", ".- "},
    {"B", "-... "},
    {"C", "-.-. "},
    {"D", "-.. "},
    {"E", ". "},
    {"F", "..-. "},
    {"G", "--. "},
    {"H", ".... "},
    {"I", ".. "},
    {"J", ".--- "},
    {"K", "-.- "},
    {"L", ".-.. "},
    {"M", "-- "},
    {"N", "-. "},
    {"O", "--- "},
    {"P", ".--. "},
    {"Q", "--.- "},
    {"R", ".-. "},
    {"S", "... "},
    {"T", "- "},
    {"U", "..- "},
    {"V", "...- "},
    {"W", ".-- "},
    {"X", "-..- "},
    {"Y", "-.-- "},
    {"Z", "--.. "},
    {" ", " "},
    {",", "--..-- "},
    {".", ".-.-.- "},
    {"?", "..--.. "},
    {"`", ".----. "}
    };
char * morse_encode(char * signal, const char * message, int unit_len, int pad_len)
{
    int i,j;
    char morse_code[100];

    morse_code[0]='\0';
    for (i = 0; i< strlen(message); i++)
    {
        for(j = 0; j< 32; j++)
        {
            if (dict[j].l[0] == message[i])
             strcat(morse_code, dict[j].code);
        }

    }
    morse_code[strlen(morse_code)] = '\0';
    /* signal */
    signal[0]='\0';
     for (i = 0; i < pad_len; i++)
    {
       strcat(signal, "0");
    }
    for (i = 0; i <strlen(morse_code); i++)
    {
        switch(morse_code[i])
        {
            case '.':
                for (j=0; j<unit_len; j++)
                    strcat(signal, "1");

                    for (j=0; j<unit_len; j++)
                        strcat(signal, "0");

            break;
            case '-':
                for (j=0; j<3*unit_len; j++)
                    strcat(signal, "1");
                    for (j=0; j<unit_len; j++)
                        strcat(signal, "0");
            break;
            case ' ':
                if (morse_code[i+1] == ' ')
                {
                    for (j=0; j<(7*unit_len) - unit_len; j++)
                        strcat(signal, "0");
                }else
                    if (morse_code[i-1] == ' ')
                        break;
                    else
                    {
                        for (j=0; j<3*unit_len-unit_len; j++)
                            strcat(signal, "0");
                    }

            break;
        }
    }
    for (i = 0; i < pad_len; i++)
    {
       strcat(signal, "0");
    }
    signal[strlen(signal)-3*unit_len]= '\0';
    return signal;
}
int unitLen (const char * signal)
{
    int i, unit_len;
    char* istr = NULL;
    char s1[2*strlen(signal)];
    char s0[strlen(signal)+3];
    char message1[100] ;
    s0[0]='\0';
    s1[0] = '\0';
    strcpy(s1, "0");
    strcpy(s0, signal);
    strcat(s1, s0);
    strcat(s1, "0");
    s1[strlen(signal)+3] = '\0';
    char s[strlen(signal)];
    s[0] = '\0';
    s[0] = '0';
    for (i = 1; i<strlen(signal)-3; i++)
    {
        s[i]='1';
        s[i+1] ='0';
        s[i+2]='\0';
        istr = strstr (s1,s);
        if (istr != NULL)
        {
            unit_len = i;
            break;
        }
    }
    return unit_len;
}

char * morse_decode(char * message, const char * signal)
{
    int unitL = unitLen(signal);
    int i,j,len;
    char c;
    char message1[100] ;
    char s[100];
    message1[0]='\0';
    s[0]='\0';
    int p  = 0;
    message[0] = '\0';
    for (j=0; j< strlen(signal); j++)
    {
        if (signal[j] != '0')
        {
            if (j == 0)
            {
                j=0;
                break;
            }
            else
            break;
        }
    }
    for (i = j; i < strlen(signal)-j; i+=len)
    {
        c = signal[i];
        if (signal[i+unitL] != c)
        {
            if(c == '1')
             strcat(message, ".");
             len = unitL;

        } else if (signal[i+3*unitL] != c)
        {
            if(c == '1')
             strcat(message, "-");
             else strcat(message, " ");
             len = unitL*3;

        }else if (signal[i+7*unitL] != c)
        {
            if(c == '0')
             strcat(message, "  ");
             len = unitL*7;
        }

    }
    i = 0;
    strcat(message, " ");
    message[strlen(message)+1] = '\0';
    int  y = 0;
    while (i<strlen(message))
    {
           s[p] = message[i];
           s[p+1]='\0';
           p++;
        if (message[i] == ' ')
        {

             for(j = 0; j<=32; j++)
            {
                if (strcmp(dict[j].code, s) == 0)
                {
                    message1[y] = dict[j].l[0];
                    y++;
                    message1[y]='\0';
                }
            }
            s[0]='\0';
            p = 0;
        }
        i++;
    }
    message = message1;
    return message;
}
int main()
{
   char outputBuffer[1000];
    struct TestCase testCaseArray[] = {
        {"HEY DUDE", 2, 0, "1100110011001100000011000000111111001100111111001111110000000000000011111100110011000000110011001111110000001111110011001100000011"},
        {"HEY DUDE", 1, 3, "00010101010001000111010111011100000001110101000101011100011101010001000"},
        {"HEY DUDE", 3, 0, "111000111000111000111000000000111000000000111111111000111000111111111000111111111000000000000000000000111111111000111000111000000000111000111000111111111000000000111111111000111000111000000000111"},
        {"HELLO KITTY", 2, 2,"0011001100110011000000110000001100111111001100110000001100111111001100110000001111110011111100111111000000000000001111110011001111110000001100110000001111110000001111110000001111110011001111110011111100"},
        {"ONE TWO THREE", 4, 0, "1111111111110000111111111111000011111111111100000000000011111111111100001111000000000000111100000000000000000000000000001111111111110000000000001111000011111111111100001111111111110000000000001111111111110000111111111111000011111111111100000000000000000000000000001111111111110000000000001111000011110000111100001111000000000000111100001111111111110000111100000000000011110000000000001111"}
    };
    char * testOutput = NULL;
    int testCaseArrayLength = __LEN(testCaseArray);
    int testIndex = 0;
    int testStatus = 0;
    struct TestCase * testCase = NULL;

    puts("ENCODE:\n");
    for (testIndex = 0; testIndex < testCaseArrayLength; testIndex++) {
        testCase = &testCaseArray[testIndex];
        outputBuffer[0] = '\0';
        testOutput = morse_encode(outputBuffer, testCase->input, testCase->unit_len, testCase->pad_len);
        testStatus = (0 == strcmp(testOutput, testCase->output));
        printf("%s // %s\n", testCase->input, __BOOLSTR(testStatus));
    }

    puts("\nDECODE:\n");
   for (testIndex = 0; testIndex < testCaseArrayLength; testIndex++) {
        testCase = &testCaseArray[testIndex];
        outputBuffer[0] = '\0';
        testOutput = morse_decode(outputBuffer, testCase->output);
        testStatus = (0 == strcmp(testOutput, testCase->input));
        printf("%s == %s // %s\n", testCase->input, testOutput, __BOOLSTR(testStatus));
}

    printf("\nEnd.\n");
    return 0;
}

