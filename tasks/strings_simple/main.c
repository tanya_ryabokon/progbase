#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <progbase.h>
#include <pbconsole.h>

int main()
{
    int len = 0;
    int i = 0;
    int y = 0;
    int k = 0;
    int x = 0;
    int q = 0;
    char vowels[] = "aeiouy";
    char str[] = "Object-oriented programming, also called OOP, is a model for writing computer programs.\n\
Before OOP, most programs were a list of instructions that acted on memory in the computer.\n\
Instead of a procedural list of actions, OOP is modeled around objects that interact with each other.\n\
Classes generate objects and define their structure, like a blueprint.\n\
The objects interact with each other to carry out the intent of the computer program.\n\
Many design patterns have been written utilizing OOP principles for code reuse.";
    char subs[strlen(str)];

    printf("\n");
    conSetAttr(BG_BLUE);
    printf("1. Output text and total number of characters in console");
    conReset();
    len = strlen(str);
    printf("\n\n");
    puts(str);
    printf("\nTotal number of characters: %i\n", len);
    printf("=======\n\n");

    conSetAttr(BG_BLUE);
    printf("2. Output text with no letters in upper case and the number of output symbols.");
    conReset();
    printf("\n\n");

    for (i = 0; i < strlen(str); i++)
    {
        if (!isupper(str[i]))
        {
            printf("%c", str[i]);
            y++;
        }
    }
    printf("\n\n");
    printf("Number of output symbols: %i\n", y);
    printf("=======\n\n");

    conSetAttr(BG_BLUE);
    printf("3. Display all sentences of text and a note on the number of characters in the output sentence.");
    conReset();
    printf("\n\n");
    y = 0;
    for (i = 0; i < strlen(str); i++)
    {
        subs[y]=str[i];
        y++;
       if(str[i] == '.')
       {
            subs[y]='\0';
            printf("%s\n", subs);
            printf("Number of characters in the sentence: %i\n", y-1);
            printf("------\n");
            subs[0]='\0';
            y = 0;
            i++;
       }
    }
    printf("=======\n\n");

    conSetAttr(BG_BLUE);
    printf("4. Display the total number of words in the text.");
    conReset();
    printf("\n\n");
    y = 0;
    for (i = 0; i < strlen(str); i++)
    {
       if(str[i] == ' ')
       {
            y++;
       }
    }
    printf("Total number of words: %i\n", y);
    printf("=======\n\n");

    conSetAttr(BG_BLUE);
    printf("5. Output all words that begin with the letter in uppercase. Also, bring the total number of these words.");
    conReset();
    printf("\n\n");
    subs[y]='\0';
    y = 0;
    k = 0;
    for (i = 0; i < strlen(str); i++)
    {
        subs[y]=str[i];
        y++;
       if(str[i] == ' ' || str[i] == ',' || str[i] == '.')
       {
            subs[y-1]='\0';
            if(isupper(subs[0]))
            {
                if(k>0)
                printf(", ");
                printf("%s", subs);
                k++;
            }
            subs[0]='\0';
            y = 0;
            i++;
       }
    }
    printf("\n");
    printf("Total number of words that begin with the letter in uppercase: %i\n", k);
    printf("=======\n\n");

    conSetAttr(BG_BLUE);
    printf("6. Output all words that have at least two vowels in a row. Also, bring the total number of these words.");
    conReset();
    printf("\n\n");
    subs[y]='\0';
    y = 0;
    k = 0;
    for (i = 0; i < strlen(str); i++)
    {
    	if(!iscntrl(str[i]))
    	{
        	subs[y]=str[i];
        	y++;
        }
       if(str[i] == ' ' || str[i] == ',' || str[i] == '.')
       {
            subs[y-1]='\0';
            for(y =0; y< strlen(subs); y++)
            {
                for(x = 0; x< strlen(vowels); x++)
                {
                    if (tolower(subs[y]) == vowels[x])
                    {
                        for(q = 0; q< strlen(vowels); q++)
                        {
                            if (tolower(subs[y+1]) == vowels[q])
                            {
                                if(k>0)
                                printf(", ");
                                printf("%s", subs);
                                k++;
                            }
                        }
                    }
                }
            }
            y=0;
            subs[0]='\0';
       }
    }
    printf("\n");
    printf("Total number of words that have at least two vowels in a row: %i\n", k);
    printf("=======\n");
    return 0;
}
