#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <progbase.h>
#include <pbconsole.h>
#include <string.h>

int main(void)
{
    int arr[10];
	int i = 0;
	int random = 0;
	int k = 0;
	int sum =0;
	double arithmMean = 0;
	int minN = -99;
	int maxN = 199;
	int max;
	int maxI = 0;
	int min;
	int minI = 0;
	int p =0;
	char s[50];
    srand(time(0));
    printf("1. An array of integers (in the range [-99..199]) of ten items:\n");
	for (i = 0; i < 10; i++)
	{
    	   random = minN + rand() % (maxN - minN);
    	    arr[i] = random;
    	    printf("%i ", arr[i]);
	}
	printf("\n\n");
	printf("2. Numbers smaller than '-25' or greater then '25' :\n");
	for (i = 0; i < 10; i++)
	{
    	if((arr[i] < -25)||(arr[i] > 25))
    	{
    		conSetAttr(BG_GREEN);
    		printf("%i", arr[i]);
    		conReset();
    		 printf(" ");
    	} else
    		printf("%i ", arr[i]);
    		
	}
	printf("\n\n");
	printf("3. Numbers smaller than '-50' or greater then '150' :\n");
	for (i = 0; i < 10; i++)
	{
    	if((arr[i] < -50)||(arr[i] > 150))
    	{
    		conSetAttr(BG_RED);
    		printf("%i", arr[i]);
    		k++;
    		sum += arr[i];
    		conReset();
    		 printf(" ");
    	} else
    		printf("%i ", arr[i]);
    		
	}
	printf("\n");
	arithmMean = (double)sum / k;
	printf("Count: %i\n", k);
	printf("Sum: %i\n", sum);
	printf("Average: %.6f\n", arithmMean);
	printf("\n");
	printf("4. Value and index of the first maximum item in the range of [-50..50] :\n");
	for (i = 0; i < 10; i++)
	{
    	if((arr[i] >= -50)&&(arr[i] <= 50))
    	{
    		if(p==0)
    		{
    			max = arr[i];
    			maxI = i;
    			p = 1;
    		}
    		if(arr[i] > max)
    		{
    			max = arr[i];
    			maxI = i;
    		}
    	} 
	}
	printf("Max: %i\n", max);
	printf("Index of 1st max: %i\n", maxI);
	printf("\n");
	printf("5. Value and index of the first minimum positive integer item of array :\n");
	p = 0;
	for (i = 0; i < 10; i++)
	{
    	if (arr[i] > 0)
    	{
    		if(p==0)
    		{
    			min = arr[i];
    			minI = i;
    			p = 1;
    		}
    		if(arr[i] < max)
    		{
    			min = arr[i];
    			minI = i;
    		}
    	} 
	}
	printf("Min: %i\n", min);
	printf("Index of 1st min: %i\n", minI);
	printf("\n");
	printf("6. An array of characters S :\n");
	s[0]='\0';
	 for (i = 0; i < 10; i++)
	{
		s[i] = abs(arr[i]) % 95 + 32; 
	} 
	s[strlen(s)] = '\0';
	puts(s);
	printf("\n");
	printf("7. All the numbers in the range [-10..10] replaced by zero :\n");
	for (i = 0; i < 10; i++)
	{
    	if((arr[i] >= -10)&&(arr[i] <= 10))
    	{
    		arr[i] = 0;
    		conSetAttr(BG_YELLOW);
    		printf("%i", arr[i]);
    		conReset();
    		printf(" ");
    	} else
    		printf("%i ", arr[i]);
	}
	printf("\n");
    return 0;
}
