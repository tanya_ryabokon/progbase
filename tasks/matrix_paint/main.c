#include <stdio.h>
#include <pbconsole.h>
#include <progbase.h>

const char colorsTable[16][2] = {
        {'0', BG_BLACK},
        {'1', BG_INTENSITY_BLACK},
        {'2', BG_RED},
        {'3', BG_YELLOW},
        {'4', BG_INTENSITY_MAGENTA},
        {'5', BG_MAGENTA},
        {'6', BG_CYAN},
        {'7', BG_INTENSITY_YELLOW},
        {'8', BG_GREEN},
        {'9', BG_INTENSITY_RED},
        {'A', BG_INTENSITY_GREEN},
        {'B', BG_INTENSITY_WHITE},
        {'C', BG_BLUE},
        {'D', BG_INTENSITY_BLUE},
        {'E', BG_WHITE},
        {'F', BG_INTENSITY_CYAN}
    };
 

char findColor(char code)
{
     int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    int colorPairIndex = 0;
    char color ='\0';
    for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
        {
            char colorPairCode = colorsTable[colorPairIndex][0];
            char colorPairColor = colorsTable[colorPairIndex][1];
            if (code == colorPairCode)
            {
                color = colorPairColor;
                break;
            }
        }
        return color;
}
int main(void) {
char image[28][28] = {
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { 'D','D','D','D','D','D','2','2','2','2','D','D','D','D','D','D','D','D','D','D','D','2','2','2','2','D','D','D' },
    { 'D','D','D','D','D','D','1','D','D','D','D','D','D','D','D','D','D','D','D','D','D','1','D','D','D','D','D','D' },
    { 'D','D','D','D','1','D','1','D','1','D','D','D','D','D','D','D','D','D','D','1','D','1','D','1','D','D','D','D' },
    { 'D','D','D','D','1','1','1','1','1','D','D','D','D','D','D','D','D','D','D','1','1','1','1','1','D','D','D','D' },
    { 'D','D','D','D','1','1','1','1','1','D','1','D','1','D','1','D','1','D','D','1','1','1','1','1','D','D','D','D' },
    { 'D','D','D','D','1','2','2','2','1','1','1','1','1','1','1','1','1','1','1','1','2','2','2','1','D','D','D','D' },
    { 'D','D','D','D','1','2','2','2','1','C','C','1','1','C','C','1','1','C','C','1','2','2','2','1','D','D','D','D' },
    { 'D','D','D','D','1','2','2','2','1','1','1','1','1','1','1','1','1','1','1','1','2','2','2','1','D','D','D','D' },
    { 'D','D','D','D','1','2','2','2','1','1','1','7','7','7','7','7','7','1','1','1','2','2','2','1','D','D','D','D' },
    { 'D','D','D','D','1','1','1','1','1','1','1','7','7','7','7','7','7','1','1','1','1','1','1','1','D','D','D','D' },
    { 'D','D','D','D','1','1','1','1','1','1','1','7','7','7','7','7','7','1','1','1','1','1','1','1','D','D','D','D' },
    { 'D','D','D','D','1','1','1','1','1','1','1','7','7','7','7','7','7','1','1','1','1','1','1','1','D','D','D','D' },
    { 'D','D','D','D','1','1','1','1','1','1','1','7','7','7','7','7','7','1','1','1','1','1','1','1','D','D','D','D' },
    { 'E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E' },
    { 'E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E' },
    { 'E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E','E' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
};
    const unsigned long MILLIS = 20;
    char colorsPalette[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
    int i = 0;
    int j=0;
    int tmp = 0;
	int n = 0;
	int q = 0;
    conClear();
    printf("Task 1. Color Palette\n");

    for (i = 0; i < colorsPaletteLength; i++)
    {
        char colorCode = '\0';
        colorCode = colorsPalette[i];
        conSetAttr(findColor(colorCode));
        putchar(' ');
    }
    conReset();
    printf("\n");
    conClear();

    printf("Task 2. Image\n");

    for (i = 0; i < 28; i++)
    {
        char colorCode = '\0';
        for (j = 0 ; j<28; j++)
        {
        conMove(j+3, i+4);
        colorCode = '\0';
        colorCode = image[j][i];
        conSetAttr(findColor(colorCode));
        putchar(' ');
        }
    }
    conReset();
    printf("\n");
    conClear();

    printf("Task 3. Sequence output image\n");

    i=14; j = 13;
   for (n = 1; n <=29; n++)
   {
    char colorCode = '\0';
    if (n == 1)
    {
        colorCode = image[i][j];
        conSetAttr(findColor(colorCode));
        conMove(i+3,j+4);
        putchar(' ');
        fflush(stdout);
        sleepMillis(MILLIS);
    }
        if (n % 2 == 0)
        {
            for (q = 1; q<n; q++)
            {
                colorCode = '\0';
                j++;
                colorCode = image[i][j];
                conSetAttr(findColor(colorCode));
                conMove(i+3,j+4);
                putchar(' ');
                fflush(stdout);
                sleepMillis(MILLIS);
            }
            for (q = 1; q<n; q++)
            {
                colorCode = '\0';
                i--;
                colorCode = image[i][j];
                conSetAttr(findColor(colorCode));
                conMove(i+3,j+4);
                putchar(' ');
                fflush(stdout);
                sleepMillis(MILLIS);
            }
        }else{
            for (q = 1; q<n; q++)
            {
                colorCode = '\0';
                j--;
                colorCode = image[i][j];
                conSetAttr(findColor(colorCode));
                conMove(i+3,j+4);
                putchar(' ');
                fflush(stdout);
                sleepMillis(MILLIS);
            }
            if (n!=29)
            for (q = 1; q<n; q++)
            {
                colorCode = '\0';
                i++;
                colorCode = image[i][j];
                conSetAttr(findColor(colorCode));
                conMove(i+3,j+4);
                putchar(' ');
                fflush(stdout);
                sleepMillis(MILLIS);
            }

        }
   }
   conClear(); 

    printf("Task 4. Transformation and output image\n");

    for (i = 0; i <28; i++)
    {
        for (j = 0 ; j< 28-i; j++)
        {
        		tmp = image[i][j];
        		image[i][j] = image[27-j][27-i];
        		image[27-j][27-i] = tmp;
        }
    }
    for (i = 0; i <28; i++)
    {
        char colorCode = '\0';
        for (j = 0 ; j<28; j++)
        {
        conMove(j+3, i+4);
        colorCode = '\0';
        colorCode = image[j][i];
        conSetAttr(findColor(colorCode));
        putchar(' ');
        }
    }
   conReset();
   conMove(i+3,j-28);
    return 0;
}
