#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

char findUppercase(const char * pStart, const char * pChar);

int main(void) {
    char str[100];
    char chr;
    printf("PLease, enter a string: ");
    fgets(str, 100, stdin);
    str[strlen(str) - 1] = '\0';
    printf("You've entered: %s\n", str);

    /* call recursive function and print it's result*/
    chr = findUppercase(str, str + strlen(str) - 1);
    printf("\nTask:\nGet the last character in uppercase or a '0' if there isn't such character: %c\n\n", chr);

    return EXIT_SUCCESS;
}

char findUppercase(const char * pStart, const char * pChar) {
    if (pStart > pChar)
        return '0';
    else if (isupper(*pChar))
        return (int)*pChar;
    else
        return findUppercase(pStart, pChar - 1);
}

