#include <stdio.h>
#include <math.h>

double F1(double y)
{
        if((y-5) != 0)
        {
            double fun = 1/tan(y-5) - sin(y);
         return fun;
        } else
        {
            double fun = 1000;
            return fun;
        }
}
double F2(double y)
{
    double fun = 2 * sin(0.5 * y);
    return fun;
}
int main()
{
	double sum, mult, div;
    double x = -10;
    while (x <= 10)
    {
        if ((x-(int)x) != 0)
        {
              printf("x = %.1f\n", x);
        } else
        {
            printf("x = %.0f\n", x);
        }
        if (F1(x) != 1000)
        {
            printf("F1(x) = %.1f\n", F1(x));
            printf("F2(x) = %.1f \n", F2(x));
            sum = F1(x) + F2(x);
            printf("F1(x) + F2(x) = %.2f\n", sum);
            mult = F1(x) * F2(x);
            printf("F1(x) * F2(x) = %.2f\n", mult);
            if (F2(x) != 0)
            {
                div = F1(x) / F2(x);
                printf("F1(x) / F2(x) = %.2f\n", div);
            } else
            {
                printf("F1(x) / F2(x) Division by zero\n");
            }
            if (F1(x) != 0)
            {
                div = F2(x) / F1(x);
                printf("F2(x) / F1(x) = %.2f\n", div);
            } else
            {
                printf("F2(x) / F1(x) Division by zero\n");
            }
        } else
        {
            printf("F1(x) Can't be computed \n");
        }
        x = x +0.5;
    }
    return 0;
}


