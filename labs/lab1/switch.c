#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define M_PI       3.14159265358979323846

int main()
{
    int a, b, temp, op, sum, dif, mod, min, max, def;
    long mult,p;
    double v, div;
    printf("Enter operation code : ");
    scanf("%i",&op);
    printf("Enter integer a: ");
    scanf("%i",&a);
    printf("Enter integer b: ");
    scanf("%i",&b);

    if (op<0)
    {
        temp =a;
        a = b;
        b = temp;
        op = -op;
    }

    switch (op){
        case 0:
        	printf("Operation: 	a + b\n");
            sum = a + b;
            printf("Sum of a and b: %i\n", sum);
        break;
        case 1:
        	printf("Operation: 	a - b\n");
            dif = a - b;
            printf("Difference of a and b: %i\n", dif);
        break;
        case 2:
        	printf("Operation: 	a * b\n");
            mult = a * b;
            printf("Multiplication of a and b: %ld\n", mult);
        break;
        case 3:
        	printf("Operation: 	a / b\n");
            div = a / b;
            printf("Quotient of a and b: %.4f\n", div);
        break;
        case 4:
        	printf("Operation: abs(a)\n");
            mod = abs(a);
            printf("Absolute value a: %i\n", mod);
        break;
        case 5:
        	printf("Operation: min(a, b)\n");
            min = a;
            if (b<a)
                min = b;
            printf("Minimum of the numbers a and b: %i\n", min);
        break;
        case 6:
        	printf("Operation: max(a, b)\n");
            max = a;
            if (b>a)
                max = b;
            printf("Maximum of the numbers a and b: %i\n", max);
        break;
        case 7:
        case 13:
        	if ((a == 0)&&(b ==0))
        		printf("Undefined\n");
        	else
        	{
        		printf("Operation: pow(a, b)\n");
            	p = pow(a,b);
            	printf("The b-th power of a: %ld\n", p);
            }
        break;
        case 8:
        	printf("Operation: var(a, b)\n");
        	if (a != 0){
            v = (6*M_PI*cos(4*a*b))/a;
            printf("Var(a,b) =  %.4f\n", v);
           }
           else
           printf("Var can't be computed\n");
        break;
        default:
        	printf("Operation: def(a, b)\n");
            def = (op % abs(a + 1)) + (op % abs(b + 1));
            printf("Def(a,b) =  %i\n", def);
        break;
    }
   return 0;
}
