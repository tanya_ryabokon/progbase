#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>


int createWin()
{
    conClear();
    int i = 0;
    conMove(10,0);
    for (i = 3; i < 120; i++)
    {
        if ((i != 71)&&(i!= 70))
        {
            conMove(14,i);
            printf("_");
            conMove(0,i);
            printf("_");
        }
    }
    for (i = 2; i < 15; i++)
    {
        conMove(i,70);
        printf("|");
        conMove(i,2);
        printf("|");
        conMove(i,71);
        printf("|");
        conMove(i,120);
        printf("|");
    }
    conMove(16,3);
    printf("Enter command or press 'h' for help: ");
    conMove(1,1);
    printf("  ");
    conMove(17,3);
    return 0;
}
int dispTask1(int arr[])
{
    createWin();
    int i = 0;
    conMove(6,10);
    for (i = 0; i < 10; i++)
    {
        printf("%i ", arr[i]);
    }
    conMove(17,3);
    return 0;
}
int dispTask2(double a[], double b[])
{
    createWin();
    int i = 0;
    conMove(5,10);
    for (i = 0; i < 10; i++)
    {
        printf("%.1f ", a[i]);
    }
    conMove(7,10);
    for (i = 0; i < 10; i++)
    {
        printf("%.1f ", b[i]);
    }
    conMove(17,3);
    return 0;
}
int dispTask3(int m[][8])
{
    createWin();
    int i = 0;
    int j = 0;
    conMove(3,10);
    for (i = 0; i < 8; i++)
    {
        for(j=0; j< 8; j++)
            printf("%i ", m[i][j]);
        conMove(4+i,10);
    }
    conMove(17,3);
    return 0;
}
int dispTask4(char* buf)
{
    createWin();
    conMove(7,5);
    printf("%s", buf);
    conMove(17,3);
    return 0;
}
int help()
{
    conMove(2,72);
    printf("List of tasks: ");
    conMove(3,72);
    printf("Task 1. One-Dimensional Array");
    conMove(4,72);
    printf("Task 2. Two One-Dimensional Arrays");
    conMove(5,72);
    printf("Task 3. Two-Dimensional array");
    conMove(6,72);
    printf("Task 4. Processing of lines in C");
    conMove(7,72);
    printf("Press 0 to exit.");
    return 0;
}
int task1(int *arr)
{
    srand(time(0));
	int i = 0;
	int minN = -100;
	int maxN = 100;
	char a;
	int min, max;
	int minI = 0;
	int maxI = 0;
	int random;
	while(1)
	{
	dispTask1(arr);
        scanf("%s", &a);
        switch (a)
        {
            case '1':
            {
                conMove(6,10);
                for (i = 0; i < 10; i++)
                {
                    random = minN + rand() % (maxN - minN);
                    arr[i] = random;
                }
                dispTask1(arr);
                break;
            }
            case '2':
            {
                conMove(6,10);
                for (i = 0; i < 10; i++)
                {
                    arr[i] = 0;
                }
                dispTask1(arr);
                break;
            }
            case '3':
            {
                dispTask1(arr);
                min = arr[0];
                for (i = 0; i < 10; i++)
                {
                    if(arr[i] < min)
                    {
                        min = arr[i];
                        minI = i;
                    }
                }
                conMove(16, 42);
                printf("Min: %i  Index of min: %i", min, minI);
                break;
            }
            case '4':
            {
                dispTask1(arr);
                int sum = 0;
                for (i = 0; i < 10; i++)
                {
                    sum += arr[i];
                }
                conMove(16, 42);
                printf("Sum: %i", sum);
                break;
            }
            case '5':
            {
                dispTask1(arr);
                int mult = 1;
                for (i = 0; i < 10; i++)
                {
                    if (arr[i]<0)
                        mult *= arr[i];
                }
                conMove(16, 42);
                printf("Product of negative elements: %i", mult);
                break;
            }
            case '6':
            {
                dispTask1(arr);
                int y=0;
                int k[10];
                int max = 0;
                int index = 0;
                int p=0;
                for (i=0;i < 10; i++)
                {
                        k[i]=0;
                }
                for (i = 0; i < 10; i++)
                {
                    for (y=0;y < 10; y++)
                    {
                        if (arr[i] == arr[y])
                            k[i]+=1;
                    }
                }
                max = k[0];
                for (i =0; i<10; i++)
                {
                    if (k[i]>max)
                    {
                        index = i;
                        max = k[i];
                    }

                }
                if (k[index]!=1)
                {
                    conMove(16, 42);
                    printf("The most common element: %i", arr[index]);
                }else
                {
                    conMove(16, 42);
                    printf("Each element appears only once");
                }
                break;
            }
             case '7':
            {
                dispTask1(arr);
                min = arr[0];
                int c =0;
                for (i = 0; i < 10; i++)
                {
                    if(arr[i] < min)
                    {
                        min = arr[i];
                        minI = i;
                    }
                }
                max = arr[0];
                for (i = 0; i < 10; i++)
                {
                    if(arr[i] > max)
                    {
                        max = arr[i];
                        maxI = i;
                    }
                }
                c = arr[maxI];
                arr[maxI] = arr[minI];
                arr[minI] = c;
                dispTask1(arr);
                break;
            }
             case '8':
            {
                dispTask1(arr);
                int c =0;
                printf("Enter integer number: ");
                scanf("%i", &c);
                for (i = 0; i < 10; i++)
                {
                    arr[i] = c*arr[i];
                }
                dispTask1(arr);
                break;
            }
            case '0':
            {
                return 0;
                break;
            }
            case 'h':
            {
                conMove(2,72);
                printf("1. To fill an array of random numbers");
                conMove(3,72);
                printf("2. Clear all the elements of the array.");
                conMove(4,72);
                printf("3. Find the minimal element and its index.");
                conMove(5,72);
                printf("4. Find the sum of all elements");
                conMove(6,72);
                printf("5. Print the product of the negative elements");
                conMove(7,72);
                printf("6. Find the first item that repeats the");
                conMove(8,72);
                printf("greatest number of times.");
                conMove(9,72);
                printf("7. Swap maximum and minimum array elements.");
                conMove(10,72);
                printf("8. Multiply all elements to the entered number.");
                conMove(11,72);
                printf("to the entered number.");
                conMove(12,72);
                printf("Press 0 to return to main menu.");
                break;
            }
            default:
            {
                conMove(16, 42);
                printf("Command not found");
                break;
            }
        }
    }
}
int task2(double *a, double *b)
{
    srand(time(0));
	int i = 0;
	int minN = -60;
	int maxN = 60;
	char q;
	int min, max;
	int minI = 0;
	int maxI = 0;
	double random1 =0;
	double random2 = 0;
	while(1)
	{
    dispTask2(a,b);
        scanf("%s", &q);
        switch (q)
        {
            case '1':
            {
                for (i = 0; i < 10; i++)
                {
                    random1 = (double)(minN + rand() % (maxN - minN))/3.0;
                    a[i] = random1;
                    random2 = (double)(minN + rand() % (maxN - minN))/3.0;
                    b[i] = random2;
                }
                dispTask2(a,b);
                break;
            }
            case '2':
            {
                conMove(6,10);
                for (i = 0; i < 10; i++)
                {
                    a[i] = 0;
                    b[i] = 0;
                }
                dispTask2(a, b);
                break;
            }
             case '3':
            {
                double c[10];
                for (i = 0; i < 10; i++)
                {
                    c[i] = (double) a[i]/b[i];
                }
                conMove(9,10);
                for (i = 0; i < 10; i++)
                {
                    printf("%.1f ", c[i]);
                }
                conMove(17,3);
                break;
            }
            case '4':
            {
                dispTask2(a,b);
                double sum1 = 0;
                double sum2 = 0;
                int num = 0;
                for (i = 0; i < 10; i++)
                {
                    sum1 += a[i];
                    sum2 += b[i];
                }
                if (sum1 > sum2)
                    num = 1;
                else  if(sum1 < sum2)
                    num = 2;
                else
                {
                    conMove(16, 42);
                    printf("Sums of the elements in the both arrays are equal");
                    break;
                }
                conMove(16, 42);
                printf("Sum in array 1: %.1f, Sum in array 2: %.1f, Number of array with max sum: %i", sum1,sum2,num);
                break;
            }
             case '5':
            {
                dispTask2(a, b);
                min = a[0];
                double c =0;
                for (i = 0; i < 10; i++)
                {
                    if(a[i] < min)
                    {
                        min = a[i];
                        minI = i;
                    }
                }
                max = b[0];
                for (i = 0; i < 10; i++)
                {
                    if(b[i] > max)
                    {
                        max = b[i];
                        maxI = i;
                    }
                }
                c = b[maxI];
                b[maxI] = a[minI];
                a[minI] = c;
                dispTask2(a, b);
                break;
            }
            case '0':
            {
                return 0;
                break;
            }
            case 'h':
            {
                conMove(2,72);
                printf("1. To fill an array of random fractional numbers");
                conMove(3,72);
                printf("2. Clear all the elements of the arrays.");
                conMove(4,72);
                printf("3. Find the value of a new array of numbers,");
                conMove(5,72);
                printf("elements of which are the ratio of elements");
                conMove(6,72);
                printf("of the 1st and 2nd array.");
                conMove(7,72);
                printf("4. Output number of array with the maximum");
                conMove(8,72);
                printf("sum of items.");
                conMove(9,72);
                printf("5. Swap minimal element of the 1st array ");
                conMove(10,72);
                printf("and maximum element of the 2nd array.");
                conMove(11,72);
                printf("Press 0 to return to main menu.");
                break;
            }
            default:
            {
                conMove(16, 42);
                printf("Command not found");
                break;
            }
        }
      }
}

int task3(int m[][8])
{
    srand(time(0));
	int i = 0;
	int j = 0;
	int minN = -10;
	int maxN = 10;
	char q;
	int min, max;
	int minI = 0;
	int maxI = 0;
	int random = 0;
	while(1)
	{
    dispTask3(m);
        scanf("%s", &q);
        switch (q)
        {
            case '1':
            {
                for(i = 0; i < 8; i++)
                    for (j = 0; j < 8; j++)
                    {
                        random = minN + rand() % (maxN - minN);
                        m[i][j] = random;
                    }
                    dispTask3(m);
                break;
            }
            case '2':
            {
                for(i = 0; i < 8; i++)
                    for (j = 0; j < 8; j++)
                    {
                        m[i][j] = 0;
                    }
                    dispTask3(m);
                break;
            }
             case '3':
            {
                int mi = 0;
                int mj = 0;
                max =m[0][0];
                for(i = 0; i < 8; i++)
                    for (j = 0; j < 8; j++)
                    {
                        if(m[i][j]> max )
                        {
                            max = m[i][j];
                            mi = i;
                            mj = j;
                        }
                    }
                dispTask3(m);
                conMove(16, 42);
                printf("Max: %i, Index of max: i = %i, j = %i", max, mi, mj);
                break;
            }
            case '4':
            {
                int sum = 0;
                for(i = 0; i < 8; i++)
                    for (j = 0; j < 8; j++)
                    {
                        if((i+j) == 7)
                            sum += m[i][j];
                    }
                dispTask3(m);
                conMove(16, 42);
                printf("Sum of elements of antidiagonal: %i", sum);
                break;
            }
             case '5':
            {
                dispTask3(m);
                printf("Enter integer column number: ");
                scanf("%i", &j);
                int sum = 0;
                for(i = 0; i < 8; i++)
                {
                        sum += m[i][j];
                }
                dispTask3(m);
                conMove(16, 42);
                printf("Sum of elements of column %i: %i", j, sum);
                break;
            }
             case '6':
            {
                int maxi = 0;
                int maxj = 0;
                int mini = 0;
                int minj = 0;
                int c = 0;
                max =m[0][0];
                for(i = 0; i < 8; i++)
                    for (j = 0; j < 8; j++)
                    {
                        if(m[i][j]> max )
                        {
                            max = m[i][j];
                            maxi = i;
                            maxj = j;
                        }
                    }
                min =m[0][0];
                for(i = 0; i < 8; i++)
                    for (j = 0; j < 8; j++)
                    {
                        if(m[i][j]< min )
                        {
                            min = m[i][j];
                            mini = i;
                            minj = j;
                        }
                    }
                c = m[maxi][maxj];
                m[maxi][maxj] = m[mini][minj];
                m[mini][minj] = c;
                dispTask3(m);
                break;
            }
            case '7':
            {
                dispTask3(m);
                int c = 0;
                printf("Enter integer index (i and j): ");
                scanf("%i %i", &i, &j);
                conMove(18,3);
                printf("Enter integer number: ");
                scanf("%i", &c);
                m[i][j]= c;
                dispTask3(m);
                break;
            }
             case '0':
            {
                return 0;
                break;
            }
             case 'h':
            {
                conMove(2,72);
                printf("1. To fill an array of random numbers");
                conMove(3,72);
                printf("2. Clear all the elements of the arrays.");
                conMove(4,72);
                printf("3. Find the maximum element and its indexes.");
                conMove(5,72);
                printf("4. Find the sum of elements of the antidiagonal.");
                conMove(6,72);
                printf("5. Find the sum of the column for a given index.");
                conMove(7,72);
                printf("6. Swap maximum and minimum elements of array.");
                conMove(8,72);
                printf("7. Change the value of element for the specified");
                conMove(9,72);
                printf("index.");
                conMove(10,72);
                printf("Press 0 to return to main menu.");
                break;
            }
            default:
            {
                conMove(16, 42);
                printf("Command not found");
                break;
            }
        }
    }
}

int task4(char* buf)
{
    srand(time(0));
	int i = 0;
	int j = 0;
	int minN = -10;
	int maxN = 10;
	int q;
	int r =0;
	char c;
	int min, max;
	int minI = 0;
	int maxI = 0;
	int random = 0;
	dispTask4(buf);
	while(1)
	{
        conMove(17,3);
        for (i=0; i < 80; i++)
        printf(" ");
        conMove(17,3);
        r = scanf("%i", &q);
        if (r == 0)
        {
            scanf("%s", &c);
            q = (int)c;
            if (q != 104)
            continue;
        }
        conMove(17,3);
        dispTask4(buf);
        conMove(17,3);
        switch (q)
        {
            case 1:
            {
                while((q = getchar()) != '\n' && q != EOF) { }
                createWin();
                buf[0]='\0';
                conMove(17,3);
                fgets(buf, 80, stdin); /*space*/
                buf[strlen(buf)]='\0';
                dispTask4(buf);
                break;
            }
            case 2:
            {
                dispTask4(buf);
                buf[0]='\0';
                dispTask4(buf);
                break;
            }
             case 3:
            {
                dispTask4(buf);
                conMove(16, 42);
                printf("Length of string: %i", strlen(buf));
                conMove(17,3);
                break;
            }
            case 4:
            {
                dispTask4(buf);
                char sbuf[80];
                int x = 0;
                int y = 0;
                int i = 0;
                int j = 0;
                sbuf[0] = '\0';
                printf("Enter begin and length of substring: ");
                scanf("%i %i", &i, &j);
                for (x = i; x < i+j; x++)
                {
                    sbuf[y] = buf[x];
                    y++;
                }
                sbuf[y]='\0';
                dispTask4(sbuf);
                break;
            }
             case 5:
            {
                dispTask4(buf);
                strcat(buf, " ");
                char word[100];
                int wordLen = 0;
                int y = 0;
                char c;
                int j = 0;
                int wordStart = 0;
                int i = 0;
                printf("Enter the character:");
                scanf("%s", &c);
                conMove(17,3);
                for (i = 0; i < strlen(buf); i++)
                {
                    char ch = buf[i];
                    if (ch == c || i+1 == strlen(buf))
                    {
                        wordLen = i - wordStart;
                        if (wordLen > 0)
                        {
                            for (j = wordStart; j < i; j++)
                            {
                                word[j - wordStart] = buf[j];
                            }
                            word[wordLen] = '\0';
                            conMove(19+y, 3);
                            puts(word);
                            y++;
                        }
                        wordStart += wordLen + 1;

                    }
                }
                break;
            }
             case 6:
            {
                char *min=NULL;
                char *wrd=NULL;
                char sbuf[80];
                strcpy(sbuf, buf);
                wrd=strtok(sbuf," .,!?");
                min=wrd;
                while(wrd!=NULL)
                {
                    wrd=strtok(NULL," .,!?");
                    if(wrd!=NULL)
                    {
                        if(strlen(wrd)<strlen(min)) min=wrd;
                    }
                }
                dispTask4(min);
                conMove(5,5);
                printf("Shortest word: ");
                break;
            }
            case 7:
            {
                dispTask4(buf);
                char *p;
                double d;
                char sbuf[80];
                char bufD[80];
                int y = 0;
                strcpy(bufD, buf);
                conMove(16, 42);
                for(p = strtok(bufD, " "); p; p = strtok(NULL, " "))
                {
                    y=0;
                    for (i = 0; i<strlen(p); i++)
                    {
                        if(isdigit(p[i]) || p[i]=='.')
                        {
                            sbuf[y] = p[i];
                            y++;
                        }
                    }
                    sbuf[y]='\0';
                        d = strtod(sbuf, NULL);
                        if (d != 0 && (d-(int)d != 0))
                        printf("%.1f ", d);

                }
                conMove(17,3);
                break;
            }
            case 8:
            {
                dispTask4(buf);
                int mult = 1;
                char *p;
                int d;
                int f = 0;
                int y =0;
                char sbuf[80];
                char bufD[80];
                strcpy(bufD, buf);
                conMove(16, 42);
                for(i = 0; i < strlen(buf); i++)
                {
                        if(isdigit(buf[i]))
                        {
                            if (buf[i+1]!='.' && !isdigit(buf[i+2]))
                            {
                            d= atoi(&buf[i]);
                            mult *= d;
                            f = 1;
                            }else
                            i+=2;
                        }
                }

                if (f == 1)
                    printf("Product of integer numbers from string: %i", mult);
                else
                    printf("There are no integer numbers in string");
                conMove(17,3);
                break;
            }
             case 0:
            {
                return 0;
                break;
            }
            case 104:
            {
                conMove(2,72);
                printf("1. To fill the string with input from the");
                conMove(3,72);
                printf("console.");
                conMove(4,72);
                printf("2. Clear string.");
                conMove(5,72);
                printf("3. Print a length of string.");
                conMove(6,72);
                printf("4. Output substring from a specified position ");
                conMove(7,72);
                printf("and specified length.");
                conMove(8,72);
                printf("5. Output list of substrings separated");
                conMove(9,72);
                printf("by specified symbol.");
                conMove(10,72);
                printf("6. Output shortest word.");
                conMove(11,72);
                printf("7. Find and display all fractional numbers.");
                conMove(12,72);
                printf("8. Find and bring the product of all integers.");
                conMove(13,72);
                printf("Press 0 to return to main menu.");
                break;
            }
            default:
            {
                createWin();
                conMove(16, 42);
                printf("Command not found");
                break;
            }
        }
    }
}
int main()
{
    char f;
    int arr[10];
    double a[10];
    double b[10];
    int m[8][8];
    char buf[80]= "String";
    int i =0;
    int j = 0;
    for (i = 0; i < 10; i++)
    {
        arr[i] = 0;
        a[i] = 0;
        b[i] = 0;
    }
    for(i = 0; i < 8; i++)
     for (j = 0; j < 8; j++)
      {
          m[i][j] = 0;
      }
    while (1)
    {
        help();
        createWin();
        scanf("%s", &f);
        switch (f)
        {
            case '1':
            {
                task1(arr);
                break;
            }
            case '2':
            {
                task2(a,b);
                break;
            }
            case '3':
            {
                task3(m);
                break;
            }
            case '4':
            {
                task4(buf);
                break;
            }
            case '0':
            {
                return 0;
                break;
            }
            default:
            {
                createWin();
                if(f != 104){
                conMove(16, 42);
                printf("Command not found");
                }
                break;
            }
        }
    }
    return 0;
}
