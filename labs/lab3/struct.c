#include "struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <progbase.h>
#include <pbconsole.h>

char* inventorToString(struct Inventor * inventor, char * str) {
    str[0]='\0';
    if (inventor != NULL)
    sprintf(str, "%i. %s %i %.2f %s in %i year.",inventor->index, inventor->name, inventor->numberOfPatents,
    inventor->productivity, inventor->p.invention, inventor->p.year);
    else return NULL;
    return str;
}

struct Inventor* stringToStruct(struct Inventor * inventor, char * str) {
    if (str[0]!='\0')
    {
        sscanf(str, "%[^0123456789\n] %d %f %[^0123456789\n] %i", inventor->name, &inventor->numberOfPatents,
        &inventor->productivity, inventor->p.invention, &inventor->p.year);
        inventor->index = 0;
    }
    else return NULL;
    return inventor;
}
struct Inventor* clearStruct(struct Inventor * inventor) {
   inventor->name[0] = '\0';
   inventor->index = 0;
   inventor->numberOfPatents = 0;
   inventor->productivity = 0;
   inventor->p.invention[0]='\0';
   inventor->p.year = 0;
    return inventor;
}
struct Inventor* addInventor(struct Inventor * inventor, struct Inventor inventors[], int* size) {

   strcpy(inventors[(*size)].name, inventor->name);
   inventors[(*size)].numberOfPatents = inventor->numberOfPatents;
   inventors[(*size)].productivity = inventor->productivity;
   strcpy(inventors[(*size)].p.invention, inventor->p.invention);
   inventors[(*size)].p.year = inventor->p.year;
   inventors[(*size)].index = *size;
   (*size)++;
    return inventors;
}

int structCmp(struct Inventor * i1 ,struct Inventor * i2)
{
    int f =0;
    if (strcmp(i1->name, i2->name) != 0)
            f =1;
    else
        if(i1->numberOfPatents != i2->numberOfPatents)
            f =1;
    else
        if(strcmp(i1->p.invention, i2->p.invention) != 0)
            f=1;
    else
        if(i1->p.year != i2->p.year)
            f =1;
    return f;
}
struct Inventor* textToArray(char* filename, struct Inventor i[], int* size)
{
    FILE * fin = fopen(filename, "r");
    char buffer[500];
    *size = -1;
    if (fin == NULL)
    {
        printf("Error opening file %s\n", filename);
        return i;
    }
    while (!feof(fin))
    {
        (*size)++;
        fgets(buffer, BUFFER_SIZE, fin);
        stringToStruct(&i[*size], buffer);
        i[*size].index = *size;
    }
    fclose(fin);
    return i;
}
char* arrayToText(char* str, struct Inventor i[], int* size)
{
    int  x = 0;
    char sbuf[500];
    str[0] = '\0';
    for (x = 0; x < *size; x++ )
    {
            inventorToString(&i[x], sbuf);
            strcat(str, sbuf);
            strcat(str, "\n\n");
            strcat(str, " ");
    }
    str[strlen(str) + 1] = '\0';
    return str;
}
void writeToFile(char* fileName, char* str)
{
    fileName[strlen(fileName)] = '\0';
     FILE * fout = fopen(fileName, "w");
     if (fout == NULL)
    {
        printf("Error opening file %s\n", fileName);
    }else
    fprintf(fout, " %s", str);
    conMove(11,2);
    printf("Changes saved.");
    fclose(fout);
}
struct Inventor* deleteFromArray(int ind, struct Inventor i[], int* size)
{
    int x;
    for (x = ind; x < *size-1; x++)
    {
        i[x] = i[x+1];
        i[x].index--;
    }
    (*size)--;
    return i;
}
struct Inventor* rewrite(int ind, struct Inventor inventors[], struct Inventor i)
{
        if(inventors != NULL)
        {
            inventors[ind] = i;
            inventors[ind].index = ind;
        } else return NULL;
    return inventors;
}
struct Inventor* rewriteField(struct Inventor * inventor, char* field , char* data)
{
    if (inventor != NULL)
    {
        if (strcmp(field, "name")==0)
        {
            strcpy(inventor->name, data);
        }else
        if (strcmp(field, "patents")==0)
        {
            int d = (int)strtod(data, NULL);
            inventor->numberOfPatents = d;
        }else
        if (strcmp(field, "productivity")==0)
        {
            float d = (float)strtod(data, NULL);
            inventor->productivity = d;
        }else
        if (strcmp(field, "invention")==0)
        {
            strcpy(inventor->p.invention, data);
        }else
        if (strcmp(field, "year")==0)
        {
            int d = (int)strtod(data, NULL);
            inventor->p.year = d;
        }
        else{
        conMove(11,2);
        printf("There isn't field '%s'", field);
        conMove(10,10);
        }
    }else
        return NULL;
        return inventor;
}

struct Inventor* findInventors(struct Inventor inventors[], int k , struct Inventor foundI[], int* size, int* foundSize)
{
    if (inventors != NULL){
    int x =0;
    *foundSize = 0;
    for(x=0; x<*size; x++)
    {
        if(inventors[x].numberOfPatents > k)
        {
            foundI[*foundSize]=inventors[x];
            (*foundSize)++;
        }
    }
    if (*foundSize == 0)
    return NULL;
    }else
        return NULL;
    return foundI;
}
