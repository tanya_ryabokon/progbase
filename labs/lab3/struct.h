enum {
    BUFFER_SIZE = 100
};
struct Inventor
{
    char name[100];
    int numberOfPatents;
    float productivity;
    struct patent
    {
        char invention[100];
        int year;
    } p;
    int index;
};
char* inventorToString(struct Inventor * inventor, char * str);
struct Inventor* stringToStruct(struct Inventor * inventor, char * str);
struct Inventor* textToArray(char* filename, struct Inventor i[], int* size);
char* arrayToText(char* str, struct Inventor i[], int* size);
void writeToFile(char* fileName, char* str);
struct Inventor* deleteFromArray(int ind, struct Inventor i[], int* size);
struct Inventor* rewrite(int ind, struct Inventor inventors[], struct Inventor i);
struct Inventor* rewriteField(struct Inventor * inventor, char* field , char* data);
struct Inventor* findInventors(struct Inventor inventors[], int k , struct Inventor foundI[], int* size, int* foundSize);
struct Inventor* addInventor(struct Inventor * inventor, struct Inventor inventors[], int* size);
struct Inventor* clearStruct(struct Inventor * inventor);
int structCmp(struct Inventor* i1 ,struct Inventor* i2);
