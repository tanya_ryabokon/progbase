#include "struct.h"
#include "cui.h"
#include "tests.h"
#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>


int main()
{
    int ind = 0;
    char  str[1000];
    char* filename = "inventors.txt";
    struct Inventor i = {"", 0, 0 , {"", 0}};
    struct Inventor inventors [80];
    struct Inventor foundI [80];
    int* size = malloc(sizeof(int));
    int* foundsize = malloc(sizeof(int));
    int q = 0;
    displayMenu();
    while(1)
    {
        scanf("%i", &q);
        switch (q)
        {

            case 1:
            {
                clearInput(q);
                displayArray(str, inventors, size);
                addMenu();
                scanf("%[^\n]", str);
                stringToStruct(&i, str);
                addInventor(&i, inventors, size);
                displayMenu();
                displayArray(str, inventors, size);
                clearStruct(&i);
                break;
            }
            case 2:
            {
                clearInput(q);
                displayMenu();
                textToArray(filename, inventors, size);
                displayArray(str, inventors, size);
                break;
            }
            case 3:
            {
                clearInput(q);
                displayMenu();
                displayArray(str, inventors, size);
                deleteMenu();
                scanf("%i", &ind);
                if (ind<=*size - 1)
                {
                    deleteFromArray(ind, inventors, size);
                    displayMenu();
                    displayArray(str, inventors, size);
                }
                else{
                    displayMenu();
                    displayArray(str, inventors, size);
                    printf("\n Index out of range of array.");
                    conMove(10,10);
                }
                break;
            }
            case 4:
            {
                clearInput(q);
                displayMenu();
                displayArray(str, inventors, size);
                rewriteMenu();
                scanf("%i", &ind);
                if (ind<=*size - 1)
                {
                    displayMenu();
                    displayArray(str, inventors, size);
                    addMenu();
                    clearInput(ind);
                    scanf("%[^\n]", str);
                    stringToStruct(&i, str);
                    rewrite(ind, inventors, i);
                    displayMenu();
                    displayArray(str, inventors, size);
                }else{
                    displayMenu();
                    displayArray(str, inventors, size);
                    printf("\n Index out of range of array.");
                    conMove(10,10);
                }
                break;
            }
            case 5:
            {
                char field[80];
                char data[80];
                clearInput(q);
                displayMenu();
                displayArray(str, inventors, size);
                rewriteMenu();
                scanf("%i", &ind);
                if (ind<=*size - 1)
                {
                    displayMenu();
                    displayArray(str, inventors, size);
                    rewriteFieldMenu();
                    clearInput(ind);
                    scanf("%s %s", field, data);
                    rewriteField(&inventors[ind], field, data);
                    displayMenu();
                    displayArray(str, inventors, size);
                }else{
                    displayMenu();
                    displayArray(str, inventors, size);
                    printf("\n Index out of range of array.");
                    conMove(10,10);
                }
                break;
            }
            case 6:
            {
                int k = 0;
                clearInput(q);
                displayMenu();
                displayArray(str, inventors, size);
                findKMenu();
                scanf("%i", &k);
                clearInput(k);
                findInventors(inventors, k, foundI, size, foundsize);
                displayMenu();
                displayArray(str, foundI, foundsize);
                break;
            }
            case 7:
            {
                char foutname[80];
                clearInput(q);
                displayMenu();
                conMove(15,2);
                printf("№  Name:   Patents:   Productivity:   Famous invention:  \n");
                printf("-----------------------------------------------------------------");
                conMove(18,2);
                puts(str);
                writeToFileMenu();
                scanf("%s", foutname);
                writeToFile(foutname, str);
                displayMenu();
                conMove(10,10);
                break;
            }
            case 8:
            {
                displayMenu();
                testItoS(inventors);
                testStoI();
                testSRewriteField();
                testSFind(inventors, size);
                printf("\n All tests pased.");
                conMove(10,10);
                break;
            }
            case 0:
            {
                displayMenu();
                puts("\n");
                return EXIT_SUCCESS;
            }
            default:
            {
                displayMenu();
                printf("\n Command not found.");
                conMove(10,10);
            }
        }
    }
    free(size);
    free(foundsize);
    return EXIT_SUCCESS;
}


