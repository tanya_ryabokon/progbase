#include <assert.h>
#include "struct.h"
#include "tests.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

void testItoS()
{
     struct Inventor i1 [6]= {
        {"Nikola Tesla ", 300, 3.5, {"Alternating current ", 1889}, 0},
        {"Benjamin Franklin ",12 , 0.15 , {"Lightning rod ", 1749}, 1},
        {"Thomas Edison ",2332 , 27.8 , {"Light bulb ", 1879}, 2},
        {"Alexander Graham Bell ",7 , 0.09 , {"Telephone ", 1876}, 3},
        {"George Westinghouse, Jr. ", 32, 0.5, {"Railway air brake ", 1868}, 4},
        {"Jerome H. Lemelson ",606 , 8.17 , {"Machine vision ", 1954}, 5}
    };
    char str[1000];
    int x = 0;
    assert(inventorToString(NULL,str) == NULL);
    assert(strcmp(inventorToString(&(i1[0]), str),"0. Nikola Tesla  300 3.50 Alternating current  in 1889 year.") == 0);
    assert(strcmp(inventorToString(&(i1[1]), str),"1. Benjamin Franklin  12 0.15 Lightning rod  in 1749 year.") == 0);
    assert(strcmp(inventorToString(&(i1[4]), str),"4. George Westinghouse, Jr.  32 0.50 Railway air brake  in 1868 year.") == 0);
    assert(strcmp(inventorToString(&(i1[5]), str),"5. Jerome H. Lemelson  606 8.17 Machine vision  in 1954 year.") == 0);

}

void testStoI()
{
    char str[1000];
    str[0]='\0';
    struct Inventor i1 = {"", 0, 0 , {"", 0}};
    struct Inventor i2 [4]= {
        {"Benjamin Franklin ",12 , 0.15 , {"Lightning rod ", 1749}, 0},
        {"Jerome H. Lemelson ",606 , 8.17 , {"Machine vision ", 1954}, 0},
        {"Thomas Edison ",2332 , 27.8 , {"Light bulb ", 1879}, 0}
    };
    assert(stringToStruct(&i1, str) == NULL);
    strcpy(str, "Benjamin Franklin 12 0.15 Lightning rod 1749");
    assert(structCmp(stringToStruct(&i1, str), &i2[0]) == 0);
    strcpy(str, "Jerome H. Lemelson 606 8.17 Machine vision 1954");
    assert(structCmp(stringToStruct(&i1, str), &i2[1]) == 0);
    strcpy(str, "Thomas Edison 2332 27.8 Light bulb 1879");
    assert(structCmp(stringToStruct(&i1, str), &i2[2]) == 0);
}


void testSRewriteField()
{
    char str[1000];
    char field[80];
    char data[80];
     struct Inventor i1 [4]= {
        {"Benjamin Franklin ",12 , 0.15 , {"Lightning rod ", 1749}, 0},
        {"Jerome H. Lemelson ",606 , 8.17 , {"Machine vision ", 1954}, 0},
        {"Thomas Edison ",2332 , 27.8 , {"Light bulb ", 1879}, 0},
        {"George Westinghouse, Jr. ", 32, 0.5, {"Railway air brake ", 1868}, 0}
    };
    struct Inventor i2 [4]= {
        {"Benjamin Franklin ",300 , 0.15 , {"Lightning rod ", 1749}, 0},
        {"Jerome H. Lemelson ",606 , 8.17 , {"Videocassette recorders", 1954}, 0},
        {"Thomas Edison ",2332 , 27.8 , {"Light bulb ", 1900}, 0},
        {"George Westinghouse, Jr. ", 32, 3.5 , {"Railway air brake ", 1868}, 0}
    };
    assert(rewriteField(NULL, field, data) == NULL);
    strcpy(field, "patents");
    strcpy(data, "300");
    assert(structCmp(rewriteField(&i1[0], field, data), &i2[0]) == 0);
    strcpy(field, "invention");
    strcpy(data, "Videocassette recorders");
    assert(structCmp(rewriteField(&i1[1], field, data), &i2[1]) == 0);
    strcpy(field, "year");
    strcpy(data, "1900");
    assert(structCmp(rewriteField(&i1[2], field, data), &i2[2]) == 0);
    strcpy(field, "productivity");
    strcpy(data, "3.5");
    assert(structCmp(rewriteField(&i1[2], field, data), &i2[2]) == 0);
}

void testSFind()
{
	int* n = NULL;
    int* size = malloc(sizeof(int));
    int* foundsize = malloc(sizeof(int));
    struct Inventor foundI [80];
     struct Inventor i1 [6]= {
        {"Nikola Tesla ", 300, 3.5, {"Alternating current ", 1889}, 0},
        {"Benjamin Franklin ",12 , 0.15 , {"Lightning rod ", 1749}, 0},
        {"Thomas Edison ",2332 , 27.8 , {"Light bulb ", 1879}, 0},
        {"Alexander Graham Bell ",7 , 0.09 , {"Telephone ", 1876}, 0},
        {"George Westinghouse, Jr. ", 32, 0.5, {"Railway air brake ", 1868}, 0},
        {"Jerome H. Lemelson ",606 , 8.17 , {"Machine vision ", 1954}, 0}
    };
    struct Inventor i2 [3]= {
        {"Nikola Tesla ", 300, 3.5, {"Alternating current ", 1889}, 0},
        {"Thomas Edison ", 2332, 27.8, {"Light bulb ", 1879}, 0},
        {"Jerome H. Lemelson ", 606, 8.17, {"Machine vision ", 1954}, 0}
    };
    *size = 6;
    assert(findInventors(NULL, 0, NULL, size, foundsize) == NULL);
    assert(memcmp(findInventors(i1, 10000, foundI, size, foundsize), n, 0) == 0);
    assert(memcmp(findInventors(i1, 0, foundI, size, foundsize), i1, *size) == 0);
    assert(memcmp(findInventors(i1, 100, foundI, size, foundsize), i2, *foundsize) == 0);
    free(size);
    free(foundsize);
}
